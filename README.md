# Nmonpy

【优势说明】

`nmonpy.exe`基于`python`开发的nmon分析工具，该工具脱离了`excel`的束缚，避免了一些分析nmon时出现的未知excel故障，特别是提高了稳定性场景nmon分析效率，能够在1-2秒的时间里分析出结果。

批量读入nmon文件，支持`linux、aix`平台下生成的nmon文件

对nmon文件按各性能指标进行逐个分析：主要是CPU利用率、内存利用率、磁盘IO、网络大小，生成一个总的测试汇总结果

同时针对各个nmon文件，还会生成对应的CSV分析结果，根据采样点输出结果值，更好的了解测试过程服务器性能变化情况，方便性能优化

根据情况，测试工程师可自行通过CSV结果绘制折线图

【使用方法描述】

把本工具拷贝至nmon结果所在路径下，直接双机`nmonpy.exe`（已编译可执行文件），可批量生成CSV测试结果

【功能增强计划】

增加log日志功能：在工具运行过程中打印运行日志

增加html折线图报表功能：根据对应的nmon文件，生成各自的html折线图报表




## 版本说明



**nmonpy1.3版本**

【更新说明】

1.3版本在python3.4环境中编辑，测试通过

编译成exe可执行文件后，在windows2003上提示  “Getfinalpathnamebyhandlew 于动态链接库 kernel32.dll ” 的问题，发现是64位环境编译的exe不能在32位操作系统上运行，解决办法：python3.8降低到3.4，32位版本。

解决了exe在盘符根目录下不能打开结果文件夹的问题


**nmonpy1.2.2版本**

【更新说明】

兼容AIX平台下nmon文件

对在目录下无nmon文件时进行代码逻辑优化

**nmonpy-forLinux1.0版本**

【更新说明】

批量分析nmon结果，生成针对各个nmon文件的CSV分析结果

仅支持linux平台下生成的nmon结果

**nmonpy-forLinux0.8版本**

【更新说明】

测试结果显示在CSV文件中，对AIX和Linux平台nmon做了判断


**nmonpy-forLinux0.5版本**

【更新说明】

CPU、内存、磁盘、网络大小等测试结果仅仅显示在cmd界面中

## 

**运行过程**：

【无nmon文件】

    “您好，欢迎使用。

      nmonpy正在寻找nmon文件，请稍等”

      ......

      ......

     “正在努力中”

      ......

      ......

      ......


     “劳驾把nmonpy部署到nmon结果所在路径下。”

     当前路径：D:\pythonwork\workspace\NmonPy 

     “没有找到nmon文件，程序稍后退出......”
     

【找到nmon文件】

    “您好，欢迎使用。”

     “nmonpy已发现目标，开始对nmon文件进行分析，请稍等。”


    主机名称            CPU利用率％   内存利用率％    磁盘（IO/s）  网络（KB/s）

    lng01                    98.73%      76.10%         4459.77        11274.41    

    ametest                  9.12%       46.99%         6.83           101.77      

    RedHatf50c12             34.13%      23.74%         4.66           11350.30    

    the-gibson               25.01%      33.16%         250.65         620.76      

    nature1043               11.19%      25.86%         394.49         2775.87     


    “共发现 5 个nmon结果，其中 4 个Linux版本、1 个AIX版本的nmon结果。


    “您好，nmonpy已完成本次nmon分析工作。” 

     最终结果路径：D:\pythonwork\workspace\NmonPy\Result

     See You！
